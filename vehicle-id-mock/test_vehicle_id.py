import pytest, webtest

from vehicle_id import vehicle_id

ENDPOINT_PREFIX = "/wanda/lookup/v1/vehicle/"

@pytest.fixture()
def authed_app():
    test_app = webtest.TestApp(vehicle_id)
    test_app.authorization = ('Basic', ('ze-user', 'secret'))
    return test_app

def test_works(authed_app):
    resp = authed_app.get(ENDPOINT_PREFIX + "10001")
    assert resp.json['regNo'] == 'regNo-10001'
    assert resp.json['companyId'] == 10001
