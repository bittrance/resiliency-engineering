import json, time

from bottle import abort, auth_basic, Bottle, request, response

vehicle_id = Bottle()

def is_ze_user(username, password):
    return username == 'batch_event_api' and password == 'eiPh9Eihae4tai5ohj0chob2Aig1shei'

@vehicle_id.get('/wanda/lookup/v1/vehicle/<serial>')
@auth_basic(is_ze_user)
def lookup_vehicle(serial):
    return {
        "regNo": "regNo-" + serial,
        "vin": "vinString-" + serial,
        "vehicleId": int(serial),
        "customer": "arriva_nl",
        "company": "company-" + serial,
        "companyId": int(serial),
    }
