# Leif resiliency testing

In order to ensure that Leif can stand up in a storm, we need some canned violence to unleash.

## Performance tests

This repo contains some load tests based on artillery.io which benchmarks Leif's latency distribution.

Install node and artillery:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
nvm install stable
npm install
```

Run a test:

```bash
npm run artillery ./load/load-leif.yaml
```

The tests typically require a certain p99 latency and a max error rate and will fail if these are violated. Please note that if you run these tests against a system with consumers (e.g. Django) you may flood the destination with nonsense data.

## Resiliency tests

These tests use chaostoolkit.org to formulate experiments to prove that Leif performs under adverse conditions, such as a software update.

Install chaostoolkit:

```bash
sudo yum install python-virtualenv python3
virtualenv -p /usr/bin/python3 chaosenv/
source ./chaosenv/bin/activate
pip install -r requirements.txt
```

Run an experiment:

```bash
chaos --verbose run ./tests/test-leif-restart.yaml
```

These expriments typically run a load test (see above) while perturbing Leif. **Currently, chaostoolkit does not fail the experiment when artillery fails during the method, so you have to run with `--verbose` and check the output from artillery.**
